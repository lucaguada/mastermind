import java.util.*;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

interface Mastermind {
  static void main(String[] args) {

  }
}

enum Outcome {InProgress, Won, Lost}

enum Peg {Black, White}

record Code(Peg... pegs) {}

record Feedback(Outcome outcome, Peg... pegs) {}

record Guess(Code code, Feedback feedback) {}

interface Aggregate<AGGREGATE extends Aggregate<AGGREGATE, EVENT>, EVENT> extends Iterable<EVENT> {
  default Stream<EVENT> stream() {return StreamSupport.stream(spliterator(), false);}

  default Iterator<EVENT> iterator() {return Collections.emptyIterator();}

  AGGREGATE append(EVENT event);
}

interface Started extends Aggregate<Game, Game.Event> {
  default boolean isStarted() {return stream().anyMatch(it -> it instanceof Joined);}
}

sealed interface Game extends Started {
  default Optional<Code> secret() {
    return stream()
      .filter(it -> it instanceof Joined)
      .map(it -> ((Joined) it).secret())
      .findFirst();
  }

  sealed interface Command {}

  sealed interface Event {}

  non-sealed interface Failed extends Event {}

  record Id(UUID id) {
    static Id random() {return new Id(UUID.randomUUID());}
  }

}

// Aggregate
record State(Stream<Event> stream) implements Game {
  @Override
  public Game append(Event event) {
    return new State(Stream.concat(stream(), Stream.of(event)));
  }
}

// Commands
record Join(Code secret, byte attempts, Set<Peg> availablePegs) implements Game.Command {}

record MakeGuess(Code code) implements Game.Command {}

// Events
record Joined(Code secret) implements Game.Event {}

record GuessMade() implements Game.Event {}


enum Handler {
  Execution;

  Game execute(Game game, Game.Command command) {
    return switch (command) {
      case Join(var secret, var attempts, var availablePegs) when !game.isStarted() -> game.append(new Game.Event.Joined(secret));
      case MakeGuess make -> game;
      default -> throw new IllegalStateException("Unexpected value: " + command);
    };
  }

  Game join(Join join) {
    return
  }
}
